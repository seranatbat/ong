import math,random
def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, x, y = egcd(b % a, a)
        return (g, y - (b // a) * x, x)

def mulinv(b, n):
    g, x, _ = egcd(b, n)
    print(g,x,_)
    if g == 1:
        return x % n

def sign(n,m):
	n=mes2num(n)
	k = n
	while (math.gcd(k,n) != 1):
		k=random.randint(2,n)
	h = n - mulinv(k,n)**2
	m = mes2num(m)
	r = n
	while (math.gcd(n,r) != 1):
                r =random.randint(2,n)
	
	s1 = (m*mulinv(r,n) + r) * mulinv(2,n) % n
	s2 = (m*mulinv(r,n) - r) * mulinv(2,n) % n
	
	return s1,s2,n,h,m

def check(s1,s2,h,n,m):
	if  (s1 * s1 + h * s2 * s2) % n == mes2num(m):
		
		return True
	else:
		print( (s1 * s1 + h * s2 * s2) % n, '\n', m)
		return False
def phi(n):
    amount = 0
    for k in range(1, n + 1):
        if math.gcd(n, k) == 1:
            amount += 1
    return amount
def mes2num(m):
	num = 0
	for i in range(len(m)):
		num += ord(m[i])*(256**i)
	return num
def num2mes(m):
	mes=''
	while( m / 256 != 0):
		mes+=chr( m % 256)
		m/=256
		return mes
def cc_send(m,m1,h):
	s1 = mulinv(2,n) * (m1*mulinv(m,n) + m ) % n 
	s2 = mulinv(2,n) * (m1*mulinv(m,n) - m ) % n
	return s1,s2
def cc_receive(n,m):
	s1,s2,n1,h,m = sign(n,m)
	print( check( s1,s2,h,n1,num2mes(m) ))
	if  (s1*s1- s2*s2 -m) % n == 0:
		return true
	else:
		return false

def main():
	n = input('\n n:\n')
	m = input('\n m \n ')
	s1,s2,n1,h,m = sign(n,m)
	print( check( s1,s2,h,n1,num2mes(m) ) )

main()
