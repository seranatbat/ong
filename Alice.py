import random
import socket

def gcd(a , b):
    while b:
        a , b = b , a % b
    return a

def rae(a, b):
    y0 = 0
    y1 = 1
    while b > 0:
        q = a / b
        r = a - q*b
        y = y0 - q*y1
        a = b
        b = r
        y0 = y1
        y1 = y
    return y0


n = 81097
k = 384

Message = random.randint(200, n-2)
while gcd(n,Message)!=1:
    Message = random.randint(200, n-2)

SecretMessage = random.randint(200, n-2)
while gcd(n,SecretMessage)!=1:
    SecretMessage = random.randint(200, n-2)

r1 = rae(n, k)
r1 = r1%n
h = (r1*r1)%n

r = rae(n, SecretMessage)
r = r % n

S1 = ((Message * r) + SecretMessage)
S1 = (S1*(n+1)/2)%n

S2 = ((Message * r) - SecretMessage)
S2 = (S2 * k*(n+1)/2)%n

sock = socket.socket()
sock.connect(('localhost', 9090))
sock.send(str(Message)+' '+str(S1)+' '+str(S2))
print('Message:' , Message)
print( 'S1:', S1)
print( 'S2', S2)
print( 'SecretMessage:' , SecretMessage)
sock.close()





