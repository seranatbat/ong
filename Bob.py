#!usr/bin/env python
import random
import socket

def gcd(a , b):
    while b:
        a , b = b , a % b
    return a

def rae(a, b):
    y0 = 0
    y1 = 1
    while b > 0:
        q = a / b
        r = a - q*b
        y = y0 - q*y1
        a = b
        b = r
        y0 = y1
        y1 = y
    return y0


n = 81097
k = 384

sock = socket.socket()
sock.bind(('', 9090))
sock.listen(1)
conn, addr = sock.accept()

f = True
print( 'addr=' , addr)
while f:
    d = conn.recv(1024)
    data = d.split(" ")
    f = False
    if not d:
        break

Message = int(data[0])
S1 = int(data[1])
S2 = int(data[2])
print( 'Message:' , Message)
print( 'S1:', S1)
print( 'S2', S2)



r1 = rae(n, k)
r1 = r1%n
h = (r1*r1)%n
f = 0
if ((S1*S1 - h*S2*S2)%n == Message):
    f = 1

print(f)
print(Message, (S1*S1 - h*S2*S2)%n)
if f==1:
    r = rae(n, S1 + S2*r1) % n
    Secret = Message*r%n
    print(Secret)

conn.close()
